<!DOCTYPE html>
<html>
<head>
	<title>PHP Ejercicio 01</title>
	<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

<?php
	
	function suma(){
	$A=4;
	$B=3;
		return $A + $B;
	}

  function resta(){
	$A=4;
	$B=3;
		return $A - $B;
	}

  function mult(){
	$A=4;
	$B=3;
		return $A * $B;
	}

 function div(){
	$A=4;
	$B=3;
		return $A / $B;
	}

  function expo(){
	$B=3;
		return exp($A.$B);
	}

?>
</head>
<body>
	<h1> 2. Crea una pagina en la que se almacenan dos numeros en variables y muestre en una tabla los valores que toman las variables y cada 		uno de los resultados de todas las operaciones aritmeticas. Operacion Valor.
	</h1>

	<table>
	  <tr>
	    <th>Variables</th>
	    <th>Operacion</th>
	    <th>Resultado</th>
	  </tr>
	  <tr>
	    <td>A = 4, B = 3</td>
	    <td> + </td>
	    <td><?php echo suma();?></td>
	  </tr>
	  <tr>
	    <td>A = 4, B = 3</td>
	    <td>-</td>
	    <td><?php echo resta();?></td>
	  </tr>
	  <tr>
	    <td>A = 4, B = 3</td>
	    <td>*</td>
	    <td><?php echo mult();?></td>
	  </tr>
	  <tr>
	    <td>A = 4, B = 3</td>
	    <td>/</td>
	    <td><?php echo div();?></td>
	  </tr>
	  <tr>
	    <td>exp B</td>
	    <td>exp</td>
	    <td><?php echo expo();?></td>
	  </tr>
	</table>

</body>
</html>